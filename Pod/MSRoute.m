//
//  MSRouter.m
//  MSModularization
//
//  Created by 郭明亮 on 2018/9/11.
//  Copyright © 2018年 郭明亮. All rights reserved.
//

#import "MSRoute.h"

@interface MSRoute ()

@property (nonatomic, strong) NSMutableDictionary *routeTable;

@end

@implementation MSRoute

+ (instancetype)route {
    static dispatch_once_t onceToken;
    static MSRoute *route = nil;
    dispatch_once(&onceToken, ^{
        route = [[self alloc]init];
    });
    return route;
}

- (instancetype)init {
    if (self = [super init]) {
        self.routeTable = [[NSMutableDictionary alloc]init];
    }
    return self;
}

- (void)registerRoute:(NSString *)url className:(NSString *)className {
    self.routeTable[url] = className;
}

- (void)pushRoute:(NSString *)url {
    [self pushRoute:url param:nil];
}

- (void)pushRoute:(NSString *)url param:(NSDictionary *)param {
    if (self.routeTable.count == 0 || url.length == 0) {
        NSLog(@"路由表为空");
        return;
    }
    
    Class pushClass = NSClassFromString(self.routeTable[url]);
    if (!pushClass) {        
        return;
    }
    
    UINavigationController *nav = nil;
    
    UIViewController *rootVC = UIApplication.sharedApplication.delegate.window.rootViewController;
    if ([rootVC isKindOfClass:[UINavigationController class]]) {
        nav = (UINavigationController *)rootVC;
    }else if([rootVC isKindOfClass:[UITabBarController class]]) {
        UITabBarController *tabBarVC = (UITabBarController *)rootVC;
        UIViewController *selectVC = [tabBarVC selectedViewController];
        if ([selectVC isKindOfClass:[UINavigationController class]]) {
            nav = (UINavigationController *)selectVC;
        }
    }
    NSAssert(nav, @"导航控制器为空");
    if (nav) {
        UIViewController *vc = [[pushClass alloc]init];
        [param enumerateKeysAndObjectsUsingBlock:^(NSString * _Nonnull key, NSString * _Nonnull obj, BOOL * _Nonnull stop) {
            [vc setValue:obj forKey:key];
        }];
        [nav pushViewController:vc animated:YES];
    }

}

@end
