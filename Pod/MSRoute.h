//
//  MSRouter.h
//  MSModularization
//
//  Created by 郭明亮 on 2018/9/11.
//  Copyright © 2018年 郭明亮. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MSRoute : NSObject

+ (instancetype)route;

// 注册路由
- (void)registerRoute:(NSString *)url className:(NSString *)className;

// push，无参数
- (void)pushRoute:(NSString *)url;

// push，带参数，参数key为属性名，value为值
- (void)pushRoute:(NSString *)url param:(NSDictionary *)param;

@end
